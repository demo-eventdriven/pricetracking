package be.jschoreels.demo.eventdriven.services.pricetracking.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Optional;

@RedisHash("Item")
public class Item implements Serializable {

    @Id
    private final String name;
    private final BigDecimal price;

    @PersistenceConstructor
    public Item(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    public Item(String name) {
        this.name = name;
        this.price = null;
    }

    public String getName() {
        return name;
    }

    public Optional<BigDecimal> getPrice() {
        return Optional.ofNullable(price);
    }
}
