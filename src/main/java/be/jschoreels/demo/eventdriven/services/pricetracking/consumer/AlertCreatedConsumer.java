package be.jschoreels.demo.eventdriven.services.pricetracking.consumer;

import be.jschoreels.demo.eventdriven.services.alerter.api.event.AlertCreated;
import be.jschoreels.demo.eventdriven.services.alerter.api.event.AlertDeleted;
import be.jschoreels.demo.eventdriven.services.pricetracking.domain.Item;
import be.jschoreels.demo.eventdriven.services.pricetracking.repository.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class AlertCreatedConsumer {

    public static Logger logger = LoggerFactory.getLogger(AlertCreatedConsumer.class);

    @Autowired
    private ItemRepository itemRepository;


    @JmsListener(containerFactory = "queueListenerFactory", destination = "Consumer.PriceTracking:ItemPriceTracked.VirtualTopic.Alert:AlertCreated")
    public void receiveAlertCreated(AlertCreated alertCreated){
        logger.info("Received event AlertCreated {}", alertCreated.toString());
        itemRepository.save(
                new Item(alertCreated.getItemId())
        );
    }


    @JmsListener(containerFactory = "queueListenerFactory", destination = "Consumer.PriceTracking:ItemPriceUntracked.VirtualTopic.Alert:AlertDeleted")
    public void receiveAlertCreated(AlertDeleted alertDeleted){
        logger.info("Received event AlertDeleted {}", alertDeleted.toString());
        itemRepository.deleteById(alertDeleted.getItemId());
    }
}
