package be.jschoreels.demo.eventdriven.services.pricetracking.batch;

import be.jschoreels.demo.eventdriven.services.pricetracking.api.event.ItemPriceDecreased;
import be.jschoreels.demo.eventdriven.services.pricetracking.api.event.ItemPriceIncreased;
import be.jschoreels.demo.eventdriven.services.pricetracking.domain.Item;
import be.jschoreels.demo.eventdriven.services.pricetracking.repository.ItemRepository;
import be.jschoreels.demo.eventdriven.services.pricetracking.service.PriceService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


@Component
public class PriceTracker {

    public static Logger logger = LoggerFactory.getLogger(PriceTracker.class);

    public static final BigDecimal MAXIMUM_THRESHOLD = BigDecimal.valueOf(3);

    @Autowired
    private PriceService priceService;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private ItemRepository itemRepository;

    public void addItemToTrack(String itemName){
        itemRepository.save(new Item(itemName, priceService.getPrice(itemName)));
    }

    public void removeItemToTrack(String itemName){
        itemRepository.deleteById(itemName);
    }

    @Scheduled(fixedRate = 2000)
    public Map<String, BigDecimal> getChangedPrices() {
        Map<String, BigDecimal> result = new HashMap<>();
        logger.debug("PriceTracker polling new Prices ...");
        for (Item entry: itemRepository.findAll()){
            BigDecimal newPrice = priceService.getPrice(entry.getName());
            BigDecimal oldPrice = entry.getPrice().orElse(newPrice);
            logger.info("Querying price for item {} gives current price at {}", entry.getName(), newPrice);
            itemRepository.save(new Item(entry.getName(), newPrice));
            if (newPrice.subtract(oldPrice).abs().compareTo(MAXIMUM_THRESHOLD) >= 0) {
                result.put(entry.getName(), newPrice);
                logger.info("Price for item {} has changed : was {}, is now {}", entry.getName(), oldPrice, newPrice);
                if (newPrice.compareTo(oldPrice) > 0){
                    jmsTemplate.convertAndSend(
                        "VirtualTopic.Price:ItemPriceIncreased",
                            ItemPriceIncreased.newBuilder()
                                .withItem(entry.getName())
                                .withNewPrice(newPrice)
                                .withOldPrice(oldPrice)
                                .build()

                    );
                } else {
                    jmsTemplate.convertAndSend(
                        "VirtualTopic.Price:ItemPriceDecreased",
                            ItemPriceDecreased.newBuilder()
                                .withItem(entry.getName())
                                .withNewPrice(newPrice)
                                .withOldPrice(oldPrice)
                                .build()

                    );
                }
            }
        }
        return result;
    }

}
