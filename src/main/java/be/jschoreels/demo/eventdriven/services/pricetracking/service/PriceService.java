package be.jschoreels.demo.eventdriven.services.pricetracking.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@Service
public class PriceService {

    public BigDecimal getPrice(String item) {
        return hashPrice(item).add(generateVariation());
    }

    private BigDecimal hashPrice(final String item) {
        return BigDecimal.valueOf((item.hashCode() % 1000) / 10.);
    }

    private BigDecimal generateVariation() {
        return BigDecimal.valueOf((Integer.valueOf(
            30 + LocalDateTime.now().format(DateTimeFormatter.ofPattern("ss"))) - 30) * 2 % 30);
        // At least 30 to avoid negative number, then loop from -60 to 60 with %30, which will cause cycles to have 2x2 periods (-30 -> 0, -30 -> 0, 0 -> 30, 0 -> 30)

    }

}
