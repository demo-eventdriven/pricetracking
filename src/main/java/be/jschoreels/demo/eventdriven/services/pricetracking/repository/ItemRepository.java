package be.jschoreels.demo.eventdriven.services.pricetracking.repository;

import be.jschoreels.demo.eventdriven.services.pricetracking.domain.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends CrudRepository<Item, String> {

}
