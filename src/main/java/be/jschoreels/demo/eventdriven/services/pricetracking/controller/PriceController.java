package be.jschoreels.demo.eventdriven.services.pricetracking.controller;

import be.jschoreels.demo.eventdriven.services.pricetracking.batch.PriceTracker;
import be.jschoreels.demo.eventdriven.services.pricetracking.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;


@RestController
public class PriceController {

    @Autowired
    private PriceService priceService;

    @Autowired
    private PriceTracker priceTracker;

    @RequestMapping("/price/{item}")
    public BigDecimal getPrice(@PathVariable(name = "item") String item){
        return priceService.getPrice(item);
    }

    @RequestMapping(value = "/price/tracker/{item}", method = RequestMethod.POST)
    public void addItemToTrack(@PathVariable(name = "item") String item){
        priceTracker.addItemToTrack(item);
    }

    @RequestMapping(value = "/price/tracker/{item}", method = RequestMethod.DELETE)
    public void removeItemToTrack(@PathVariable(name = "item") String item){
        priceTracker.removeItemToTrack(item);
    }

    @RequestMapping("/")
    public String health(){
        return "OK";
    }

}
